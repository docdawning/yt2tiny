FROM ubuntu:22.04
RUN apt-get update && \
    apt-get -y install ffmpeg youtube-dl yt-dlp git

RUN git clone https://github.com/docdawning/ffmpeg_squish.git /opt/ffmpeg_squish.git && \
    chmod +x /opt/ffmpeg_squish.git/bash/ffmpeg_squish.sh && \
    ln -s /opt/ffmpeg_squish.git/bash/ffmpeg_squish.sh /usr/bin/

RUN echo 'Welcome. To download use "yt-dlp". To transcode use "ffmpeg_squish."\n\n' > /etc/motd && \
    echo '[ ! -z "$TERM" -a -r /etc/motd ] && cat /etc/issue && cat /etc/motd' >> /etc/bash.bashrc

RUN date > /docker_image_build_datetime.txt
